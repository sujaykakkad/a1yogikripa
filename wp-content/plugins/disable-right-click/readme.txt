﻿=== Disable Right Click ===
Contributors: shrinitech 
Donate link: http://masterblogster.com/plugins/disable-right-click
Tags: disable right click, avoid right click, prevent right click, no right click, avoid content theft, protect blog content
Requires at least: 3.0
Tested up to: 3.6
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Disable right click plugin prevents right click on your WordPress website and blog which avoids copying website content and source code up to some extent.

For more information check <a href="http://masterblogster.com/plugins/disable-right-click"> plugin homepage</a>