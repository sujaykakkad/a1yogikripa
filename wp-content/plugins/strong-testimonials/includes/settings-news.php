<?php
/**
 * Settings > News page.
 *
 * @since 1.15.5
 * @package Strong_Testimonials
 */
?>
<div class="strong-news">

<h3>Version 1.15: Growing Pains</h3>

<p>
There were some headaches with this update. It was necessary to improve compatibility with drag-and-drop page editors like the excellent Page Builder by SiteOrigin</a> that handle shortcodes and widgets differently. I appreciate your patience and participation as I worked through the problems and deepened my understanding of WordPress. Many thanks to the people who spoke up and let me see the problems "in the wild."
</p>

<p>
Here are the plugins and themes I have tested this version with so far:
</p>
<ol>
<li><a href="https://wordpress.org/plugins/siteorigin-panels/" target="_blank" rel="nofollow">Page Builder by SiteOrigin</a></li>
<li><a href="http://www.kriesi.at/theme-overview" target="_blank" rel="nofollow">Avia Framework & Enfold theme</a></li>
<li><a href="https://thethemefoundry.com/wordpress-themes/make/" target="_blank" rel="nofollow">Make theme</a></li>
<li><a href="http://www.elegantthemes.com/gallery/divi/" target="_blank" rel="nofollow">Elegant Themes Page Builder & Divi theme by Elegant Themes</a></li>
<li><a href="http://cyberchimps.com/responsive-theme/" target="_blank" rel="nofollow">Responsive theme by CyberChimps</a></li>
</ol>

<p><a href="https://www.wpmission.com/contact" target="_blank">Contact me</a> if you have any problems with 1.15 or to suggest a page builder to test.</p>

<p>For sites that don't use page builders, reverting to 1.14.5 is an option too. Aside from the compatibility improvements, there are no new features in 1.15. Simply delete this plugin (you won't lose any data or settings), download the 1.14.5 zip file <a href="https://wordpress.org/plugins/strong-testimonials/developers/" target=_blank">here</a>, install it and ignore the update notice.</p>


<h3>Version 1.16: Moving Forward</h3>

<p>
I have taken the feedback to heart. The <code>[strong]</code> shortcode is either too complex or half-baked, depending on your viewpoint.
</p>

<p>
Version 1.16 will introduce views which will replace the shortcode "language" with <code>[strong view="1"]</code> and all the settings will be in admin. This has been the goal from the start. It is a better approach for both you and me. Yes, you will have to take a few minutes to convert your existing <code>[strong]</code> and <code>[wpmtst-*]</code> shortcodes. And, of course, I will be here to help.
</p>

<h3>Version 2.0</h3>

<p>
Strong is my project for learning plugin development and is nowhere near "mature". I do not believe in "if it ain't broke, don't fix it". I do not believe in "lite" versions. This plugin will continue to evolve. And I will continue to support you; as much as you allow, anyway.
</p>

<p>
Version 2.0 will use more modern development methods and allow deeper customization and integration with themes and plugins. It will be stronger.
</p>

<p>
<em>If you have not already, please consider posting a rating and casting a compatibility vote on <a href="https://wordpress.org/plugins/strong-testimonials/" target="_blank" rel="nofollow">wordpress.org</a>.</em>
</p>

<p>Thanks for choosing Strong Testimonials.</p>

<p>
<strong>Chris Dillon</strong><br>
Founder, <a href="https://www.wpmission.com" target="_blank">WP Mission</a><br>
</p>

</div>
